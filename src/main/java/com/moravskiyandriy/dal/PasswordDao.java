package com.moravskiyandriy.dal;

import com.moravskiyandriy.entities.User;

import java.sql.SQLException;

public interface PasswordDao {
    String getUserPassword(User user) throws SQLException;

    void setUserPassword(User user, char[] password) throws SQLException;

    void updateUserPassword(User user, char[] password) throws SQLException;
}
