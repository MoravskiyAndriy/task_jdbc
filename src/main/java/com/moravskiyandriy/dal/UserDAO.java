package com.moravskiyandriy.dal;

import com.moravskiyandriy.entities.User;

import java.sql.SQLException;

public interface UserDAO {
    void add(User user) throws SQLException;

    User getUserByPhoneNumber(String number) throws SQLException;

    void update(User user) throws SQLException;
}
