package com.moravskiyandriy.dal;

import com.moravskiyandriy.entities.Currency;

import java.sql.SQLException;
import java.util.List;

public interface CurrencyDAO {
    Currency getCurrencyByAbbreviation(String abbreviation) throws SQLException;

    void update(Currency currency) throws SQLException;

    void add(Currency currency) throws SQLException;

    void delete(Currency currency) throws SQLException;

    List<Currency> getAllCurrency() throws SQLException;
}
