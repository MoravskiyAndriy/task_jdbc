package com.moravskiyandriy.dal;

import com.moravskiyandriy.entities.Credit;
import com.moravskiyandriy.entities.User;

import java.sql.SQLException;
import java.util.List;

public interface CreditDAO {
    List<Credit> getUserCreditsById(int id) throws SQLException;

    int getUserCreditsQuantityById(int id) throws SQLException;

    void delete(Credit credit) throws SQLException;

    void add(User user, Credit credit) throws SQLException;

    void update(Credit credit) throws SQLException;

    void addCreditToUser(User user, Credit credit) throws SQLException;

    int getLastCreditId() throws SQLException;
}
