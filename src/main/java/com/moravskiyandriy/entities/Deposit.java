package com.moravskiyandriy.entities;

import java.math.BigDecimal;
import java.sql.Date;

public class Deposit {
    private int id;
    private int userId;
    private BigDecimal sum;
    private Currency currency;
    private BigDecimal rate;
    private Date openingDate;
    private Date closingDate;

    public Deposit() {
    }

    public Deposit(int userId, BigDecimal sum, Currency currency, BigDecimal rate, Date opening_date, Date closing_date) {
        this.userId = userId;
        this.sum = sum;
        this.currency = currency;
        this.rate = rate;
        this.openingDate = opening_date;
        this.closingDate = closing_date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public BigDecimal getSum() {
        return sum;
    }

    public void setSum(BigDecimal sum) {
        this.sum = sum;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public Date getOpeningDate() {
        return openingDate;
    }

    public void setOpeningDate(Date openingDate) {
        this.openingDate = openingDate;
    }

    public Date getClosingDate() {
        return closingDate;
    }

    public void setClosingDate(Date closingDate) {
        this.closingDate = closingDate;
    }

    @Override
    public String toString() {
        return ") Deposit{sum: " + sum +
                "(" + currency.getCurrencyVal() +
                ") rate:" + rate +
                "%[ " + openingDate +
                " to " + closingDate +
                "]}";
    }
}
