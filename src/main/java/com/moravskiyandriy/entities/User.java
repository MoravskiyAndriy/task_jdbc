package com.moravskiyandriy.entities;

import java.util.List;

public class User {
    private int id;
    private String name;
    private String surname;
    private String patronymic;
    private String email;
    private String phoneNumber;
    private String settlement;
    private String street;
    private int houseNumber;
    private int flatNumber = 0;
    private List<Account> accounts;
    private List<Deposit> deposits;
    private List<Credit> credits;

    public User() {
    }

    public User(String name, String surname, String patronymic, String email, String phoneNumber,
                String settlement, String street, int houseNumber, int flatNumber,
                List<Account> accounts, List<Deposit> deposits, List<Credit> credits) {
        this.name = name;
        this.surname = surname;
        this.patronymic = patronymic;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.settlement = settlement;
        this.street = street;
        this.houseNumber = houseNumber;
        this.flatNumber = flatNumber;
        this.accounts = accounts;
        this.credits = credits;
        this.deposits = deposits;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getSettlement() {
        return settlement;
    }

    public void setSettlement(String settlement) {
        this.settlement = settlement;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(int houseNnumber) {
        this.houseNumber = houseNnumber;
    }

    public int getFlatNumber() {
        return flatNumber;
    }

    public void setFlatNumber(int flatNumber) {
        this.flatNumber = flatNumber;
    }

    public List<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<Account> accounts) {
        this.accounts = accounts;
    }

    public List<Credit> getCredits() {
        return credits;
    }

    public void setCredits(List<Credit> credits) {
        this.credits = credits;
    }

    public List<Deposit> getDeposits() {
        return deposits;
    }

    public void setDeposits(List<Deposit> deposits) {
        this.deposits = deposits;
    }

    @Override
    public String toString() {
        return "User{" +
                "\nid=" + id +
                ", \nname='" + name + '\'' +
                ", \nsurname='" + surname + '\'' +
                ", \npatronymic='" + patronymic + '\'' +
                ", \nemail='" + email + '\'' +
                ", \nphoneNumber='" + phoneNumber + '\'' +
                ", \nsettlement='" + settlement + '\'' +
                ", \nstreet='" + street + '\'' +
                ", \nhouseNumber=" + houseNumber +
                ", \nflatNumber=" + flatNumber +
                ", \naccounts=" + accounts +
                '}';
    }
}
