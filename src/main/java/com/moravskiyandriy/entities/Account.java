package com.moravskiyandriy.entities;

import java.math.BigDecimal;

public class Account {
    private int userId;
    private Long number;
    private BigDecimal balance;
    private Currency currency;

    public Account() {
    }

    public Account(int userId, Long number, BigDecimal balance, Currency currency) {
        this.userId = userId;
        this.number = number;
        this.balance = balance;
        this.currency = currency;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    @Override
    public String toString() {
        return ") Account{number=" + number +
                ", balance=" + balance +
                " " + currency.getCurrencyVal() +
                "}";
    }
}
