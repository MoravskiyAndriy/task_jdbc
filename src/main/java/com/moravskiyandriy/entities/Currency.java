package com.moravskiyandriy.entities;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Currency {
    private int id;
    private String currencyVal;
    private BigDecimal toUAHModifier;

    public Currency() {
    }

    public Currency(int id, String currencyVal, BigDecimal toUAHModifier) {
        this.id = id;
        this.currencyVal = currencyVal;
        this.toUAHModifier = toUAHModifier;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCurrencyVal() {
        return currencyVal;
    }

    public void setCurrencyVal(String currencyVal) {
        this.currencyVal = currencyVal;
    }

    public BigDecimal getToUAHModifier() {
        return toUAHModifier;
    }

    public void setToUAHModifier(BigDecimal toUAHModifier) {
        this.toUAHModifier = toUAHModifier;
    }

    @Override
    public String toString() {
        if(toUAHModifier.compareTo(new BigDecimal(1))>=0){
            return "";
        }
        return "1 "+currencyVal+" = "+new BigDecimal(1).divide(toUAHModifier,2, RoundingMode.HALF_UP).
                setScale(2, RoundingMode.HALF_UP)+" UAH";
    }
}
