package com.moravskiyandriy.entities;

import java.math.BigDecimal;

public class Operation {
    private int id;
    private String operationType;
    private BigDecimal commission;

    public Operation() {
    }

    public Operation(int id, String operationType, BigDecimal commission) {
        this.id = id;
        this.operationType = operationType;
        this.commission = commission;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOperationType() {
        return operationType;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    public BigDecimal getCommission() {
        return commission;
    }

    public void setCommission(BigDecimal commission) {
        this.commission = commission;
    }

    @Override
    public String toString() {
        return "Operation{" +
                "\nid=" + id +
                ", \noperationType='" + operationType + '\'' +
                ", \ncommission=" + commission +
                '}';
    }
}
