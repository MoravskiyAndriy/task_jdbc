package com.moravskiyandriy.bankdeposit.depositfabric.implementations;

import com.moravskiyandriy.bankdeposit.deposit.BankDeposit;
import com.moravskiyandriy.bankdeposit.deposit.DepositType;
import com.moravskiyandriy.bankdeposit.deposit.implementations.CommonDepositLong;
import com.moravskiyandriy.bankdeposit.deposit.implementations.CommonDepositShort;
import com.moravskiyandriy.bankdeposit.depositfabric.DepositSupplier;
import com.moravskiyandriy.entities.Currency;
import com.moravskiyandriy.entities.User;

import java.math.BigDecimal;
import java.sql.Date;

public class CommonDepositSupplier extends DepositSupplier {
    @Override
    protected BankDeposit createDeposit(DepositType type, User user, BigDecimal sum, Currency currency) {
        if (type == DepositType.SHORT) {
            return new CommonDepositShort(user.getId(), sum, currency, new Date(new java.util.Date().getTime()));
        } else {
            return new CommonDepositLong(user.getId(), sum, currency, new Date(new java.util.Date().getTime()));
        }
    }
}
