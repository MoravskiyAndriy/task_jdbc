package com.moravskiyandriy.bankdeposit.depositfabric;

import com.moravskiyandriy.bankdeposit.deposit.BankDeposit;
import com.moravskiyandriy.bankdeposit.deposit.DepositType;
import com.moravskiyandriy.entities.Currency;
import com.moravskiyandriy.entities.User;

import java.math.BigDecimal;

public abstract class DepositSupplier {
    protected abstract BankDeposit createDeposit(DepositType type, User user, BigDecimal sum, Currency currency);

    public BankDeposit getBankDeposit(DepositType type, User user, BigDecimal sum, Currency currency) {
        return createDeposit(type, user, sum, currency);
    }
}
