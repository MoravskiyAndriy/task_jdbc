package com.moravskiyandriy.bankdeposit.deposit.implementations;

import com.moravskiyandriy.bankdeposit.deposit.DepositConstants;
import com.moravskiyandriy.entities.Currency;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.Optional;

public class BonusDepositShort extends GeneralBankDeposit {
    private static final BigDecimal SHORT_RATE = Optional.
            ofNullable(getProperties().getProperty("SHORT_RATE")).
            map(BigDecimal::new).orElse(DepositConstants.SHORT_RATE);
    private static final int BONUS_SHORT_TERM = Optional.
            ofNullable(getProperties().getProperty("SHORT_BONUS_DEPOSIT_TERM")).
            map(Integer::valueOf).orElse(DepositConstants.SHORT_BONUS_DEPOSIT_TERM);
    private static final BigDecimal SHORT_BONUS= Optional.
            ofNullable(getProperties().getProperty("SHORT_BONUS")).
            map(BigDecimal::new).orElse(DepositConstants.SHORT_BONUS);

    public BonusDepositShort(int userId, BigDecimal sum, Currency currency, Date openingDate) {
        super(userId, sum, currency, openingDate);
        setRate(SHORT_RATE.add(SHORT_BONUS));
        setTimeInDays(BONUS_SHORT_TERM);
    }
}
