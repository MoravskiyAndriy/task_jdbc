package com.moravskiyandriy.bankdeposit.deposit.implementations;

import com.moravskiyandriy.bankdeposit.deposit.DepositConstants;
import com.moravskiyandriy.entities.Currency;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.Optional;

public class CommonDepositShort extends GeneralBankDeposit {
    private static final BigDecimal SHORT_RATE = Optional.
            ofNullable(getProperties().getProperty("SHORT_RATE")).
            map(BigDecimal::new).orElse(DepositConstants.SHORT_RATE);
    private static final int COMMON_SHORT_TERM = Optional.
            ofNullable(getProperties().getProperty("SHORT_COMMON_DEPOSIT_TERM")).
            map(Integer::valueOf).orElse(DepositConstants.SHORT_COMMON_DEPOSIT_TERM);

    public CommonDepositShort(int userId, BigDecimal sum, Currency currency, Date openingDate) {
        super(userId, sum, currency, openingDate);
        setRate(SHORT_RATE);
        setTimeInDays(COMMON_SHORT_TERM);
    }
}
