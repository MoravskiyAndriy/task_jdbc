package com.moravskiyandriy.bankdeposit.deposit.implementations;

import com.moravskiyandriy.bankdeposit.deposit.DepositConstants;
import com.moravskiyandriy.entities.Currency;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.Optional;

public class CommonDepositLong extends GeneralBankDeposit {
    private static final BigDecimal LONG_RATE = Optional.
            ofNullable(getProperties().getProperty("LONG_RATE")).
            map(BigDecimal::new).orElse(DepositConstants.LONG_RATE);
    private static final int COMMON_LONG_TERM = Optional.
            ofNullable(getProperties().getProperty("LONG_COMMON_DEPOSIT_TERM")).
            map(Integer::valueOf).orElse(DepositConstants.LONG_COMMON_DEPOSIT_TERM);

    public CommonDepositLong(int userId, BigDecimal sum, Currency currency, Date openingDate) {
        super(userId, sum, currency, openingDate);
        setRate(LONG_RATE);
        setTimeInDays(COMMON_LONG_TERM);
    }
}
