package com.moravskiyandriy.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Optional;
import java.util.Properties;

public class Util {
    private static final Logger logger = LogManager.getLogger(Util.class);
    private static final String DB_URL = Optional.ofNullable(getProperties()
            .getProperty("DB_URL")).orElse(DBConstants.DB_URL);
    private static final String DB_NAME = Optional.ofNullable(getProperties()
            .getProperty("DB_NAME")).orElse(DBConstants.DB_NAME);
    private static final String DB_PASSWORD = Optional.ofNullable(getProperties()
            .getProperty("DB_PASSWORD")).orElse(DBConstants.DB_PASSWORD);

    protected static Connection getConnection() {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(DB_URL, DB_NAME, DB_PASSWORD);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }

    private static Properties getProperties() {
        Properties prop = new Properties();
        try (InputStream input = Util.class
                .getClassLoader().getResourceAsStream("config")) {
            if (input != null) {
                prop.load(input);
            }
        } catch (NumberFormatException ex) {
            logger.warn("NumberFormatException found.");
        } catch (IOException ex) {
            logger.warn("IOException found.");
        }
        return prop;
    }
}
