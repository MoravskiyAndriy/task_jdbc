package com.moravskiyandriy.services;

import com.moravskiyandriy.dal.OperationDAO;
import com.moravskiyandriy.entities.Operation;
import com.moravskiyandriy.utils.Util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class OperationService extends Util implements OperationDAO {
    @Override
    public Operation getOperationById(int id) throws SQLException {
        Connection connection = getConnection();
        PreparedStatement preparedStatement = null;
        Operation operation = new Operation();
        String sql = "SELECT * FROM operation WHERE id=?";
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            operation.setId(resultSet.getInt("id"));
            operation.setOperationType(resultSet.getString("operation_type"));
            operation.setCommission(resultSet.getBigDecimal("commission"));
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) preparedStatement.close();
            if (connection != null) connection.close();
        }
        return operation;
    }

    @Override
    public void update(Operation operation) throws SQLException {
        Connection connection = getConnection();
        PreparedStatement preparedStatement = null;
        String sql = "UPDATE operation SET" +
                "id=?,operation_type=?,commission=? WHERE id=?;";
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(4, operation.getId());
            preparedStatement.setInt(1, operation.getId());
            preparedStatement.setString(2, operation.getOperationType());
            preparedStatement.setBigDecimal(3, operation.getCommission());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) preparedStatement.close();
            if (connection != null) connection.close();
        }
    }

    public void add(Operation currency) throws SQLException {
        Connection connection = getConnection();
        PreparedStatement preparedStatement = null;
        String sql = "INSERT INTO operation " +
                "(id,operation_type,commission) " +
                "VALUES (?,?,?)";
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, currency.getId());
            preparedStatement.setString(2, currency.getOperationType());
            preparedStatement.setBigDecimal(3, currency.getCommission());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) preparedStatement.close();
            if (connection != null) connection.close();
        }
    }

    public void delete(Operation operation) throws SQLException {
        Connection connection = getConnection();
        PreparedStatement preparedStatement = null;
        String sql = "DELETE FROM operation WHERE id=?";
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, operation.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) preparedStatement.close();
            if (connection != null) connection.close();
        }
    }
}
