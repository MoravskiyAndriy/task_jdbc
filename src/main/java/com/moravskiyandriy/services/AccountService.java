package com.moravskiyandriy.services;

import com.moravskiyandriy.dal.AccountDAO;
import com.moravskiyandriy.entities.Account;
import com.moravskiyandriy.entities.Currency;
import com.moravskiyandriy.utils.Util;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class AccountService extends Util implements AccountDAO {
    @Override
    public List<Account> getUserAccountsById(int id) throws SQLException {
        Connection connection = getConnection();
        List<Account> accounts = new ArrayList<>();
        Account account;
        PreparedStatement preparedStatement = null;
        String sql = "SELECT * FROM account JOIN currency ON account.currency_id=currency.id WHERE user_id=?;";
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                account = formAccount(id, resultSet);
                accounts.add(account);
            }
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) preparedStatement.close();
            if (connection != null) connection.close();
        }
        return accounts;
    }

    private Account formAccount(int id, ResultSet resultSet) throws SQLException {
        Account account;
        account = new Account();
        Currency currency = new Currency();
        account.setNumber(resultSet.getLong("number"));
        account.setBalance(resultSet.getBigDecimal("balance"));
        account.setUserId(id);
        currency.setId(resultSet.getInt("currency_id"));
        currency.setCurrencyVal(resultSet.getString("currency_val"));
        currency.setToUAHModifier(resultSet.getBigDecimal("to_UAH_modifier"));
        account.setCurrency(currency);
        return account;
    }

    @Override
    public Account getAccountByNumber(Long number) throws SQLException {
        Connection connection = getConnection();
        Account account = new Account();
        PreparedStatement preparedStatement = null;
        String sql = "SELECT * FROM account JOIN currency ON account.currency_id=currency.id WHERE number=?;";
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setLong(1, number);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                account = formAccount(resultSet.getInt("user_id"), resultSet);
            }
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) preparedStatement.close();
            if (connection != null) connection.close();
        }
        return account;
    }

    @Override
    public int getUserAccountsQuantityById(int id) throws SQLException {
        Connection connection = getConnection();
        PreparedStatement preparedStatement = null;
        int quantity = 0;
        String sql = "SELECT count(*) as number FROM account where user_id=?;";
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            quantity = resultSet.getInt("number");
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) preparedStatement.close();
            if (connection != null) connection.close();
        }
        return quantity;
    }

    @Override
    public Long getLastAccountNumber() throws SQLException {
        Connection connection = getConnection();
        Statement statement = null;
        Long accountNumber = 0L;
        String sql = "SELECT * FROM account ORDER BY user_id DESC limit 1;";
        try {
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);
            resultSet.next();
            accountNumber = resultSet.getLong("number");
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null) statement.close();
            if (connection != null) connection.close();
        }
        return accountNumber;
    }

    @Override
    public void update(Account account) throws SQLException {
        Connection connection = getConnection();
        PreparedStatement preparedStatement = null;
        String sql = "UPDATE account SET" +
                " user_id=?,number=?,balance=?,currency_id=? WHERE number=?;";
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setLong(5, account.getNumber());
            preparedStatement.setInt(1, account.getUserId());
            preparedStatement.setLong(2, account.getNumber());
            preparedStatement.setBigDecimal(3, account.getBalance());
            preparedStatement.setInt(4, account.getCurrency().getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) preparedStatement.close();
            if (connection != null) connection.close();
        }
    }

    @Override
    public void delete(Account account) throws SQLException {
        Connection connection = getConnection();
        PreparedStatement preparedStatement = null;
        String sql = "DELETE FROM account WHERE number=?;";
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setLong(1, account.getNumber());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) preparedStatement.close();
            if (connection != null) connection.close();
        }
    }

    @Override
    public void add(Account account) throws SQLException {
        Connection connection = getConnection();
        PreparedStatement preparedStatement = null;
        String sql = "INSERT INTO account " +
                "(user_id,number,balance,currency_id) " +
                "VALUES (?,?,?,?);";
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, account.getUserId());
            preparedStatement.setLong(2, account.getNumber());
            preparedStatement.setBigDecimal(3, new BigDecimal(0));
            preparedStatement.setInt(4, account.getCurrency().getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) preparedStatement.close();
            if (connection != null) connection.close();
        }
    }
}
