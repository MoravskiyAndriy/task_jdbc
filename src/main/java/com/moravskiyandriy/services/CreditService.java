package com.moravskiyandriy.services;

import com.moravskiyandriy.dal.CreditDAO;
import com.moravskiyandriy.entities.Credit;
import com.moravskiyandriy.entities.Currency;
import com.moravskiyandriy.entities.User;
import com.moravskiyandriy.utils.Util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CreditService extends Util implements CreditDAO {
    @Override
    public List<Credit> getUserCreditsById(int id) throws SQLException {
        Connection connection = getConnection();
        List<Credit> credits = new ArrayList<>();
        PreparedStatement preparedStatement = null;
        String sql = "select * from user join user_has_credit on user.id=user_has_credit.user_id " +
                "join credit on user_has_credit.credit_id=credit.id join currency on credit.currency_id=currency.id" +
                " where user.id=?";
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Credit credit = formCredit(resultSet);
                credits.add(credit);
            }
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) preparedStatement.close();
            if (connection != null) connection.close();
        }
        return credits;
    }

    private Credit formCredit(ResultSet resultSet) throws SQLException {
        Credit credit = new Credit();
        Currency currency = new Currency();
        credit.setId(resultSet.getInt("credit_id"));
        credit.setSum(resultSet.getBigDecimal("sum"));
        credit.setRate(resultSet.getBigDecimal("rate"));
        credit.setOpeningDate(resultSet.getDate("opening_date"));
        credit.setClosingDate(resultSet.getDate("closing_date"));
        currency.setId(resultSet.getInt("currency_id"));
        currency.setCurrencyVal(resultSet.getString("currency_val"));
        currency.setToUAHModifier(resultSet.getBigDecimal("to_UAH_modifier"));
        credit.setCurrency(currency);
        return credit;
    }

    @Override
    public int getUserCreditsQuantityById(int id) throws SQLException {
        Connection connection = getConnection();
        PreparedStatement preparedStatement = null;
        int quantity = 0;
        String sql = "SELECT count(*) as number FROM user_has_credit where user_id=?;";
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            quantity = resultSet.getInt("number");
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) preparedStatement.close();
            if (connection != null) connection.close();
        }
        return quantity;
    }

    @Override
    public void delete(Credit credit) throws SQLException {
        Connection connection = getConnection();
        PreparedStatement preparedStatement = null;
        String sql = "DELETE FROM user_has_credit WHERE credit_id=?;";
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, credit.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) preparedStatement.close();
            if (connection != null) connection.close();
        }
        Connection connection2 = getConnection();
        PreparedStatement preparedStatement2 = null;
        String sql2 = "DELETE FROM credit WHERE id=?;";
        try {
            preparedStatement2 = connection2.prepareStatement(sql2);
            preparedStatement2.setInt(1, credit.getId());
            preparedStatement2.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement2 != null) preparedStatement2.close();
            if (connection2 != null) connection2.close();
        }
    }

    @Override
    public void add(User user, Credit credit) throws SQLException {
        addToCreditTable(credit);
        addToUserHasCreditTable(user);
    }

    private void addToUserHasCreditTable(User user) throws SQLException {
        Connection connection = getConnection();
        PreparedStatement preparedStatement = null;
        String sql = "INSERT INTO user_has_credit " +
                "(user_id,credit_id) VALUES (?,?);";
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, user.getId());
            preparedStatement.setInt(2, getLastCreditId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) preparedStatement.close();
            if (connection != null) connection.close();
        }
    }

    private void addToCreditTable(Credit credit) throws SQLException {
        Connection connection2 = getConnection();
        PreparedStatement preparedStatement2 = null;
        String sql2 = "INSERT INTO credit " +
                "(sum,currency_id,rate,opening_date,closing_date) " +
                "VALUES (?,?,?,?,?);";
        try {
            preparedStatement2 = connection2.prepareStatement(sql2);
            preparedStatement2.setBigDecimal(1, credit.getSum());
            preparedStatement2.setInt(2, credit.getCurrency().getId());
            preparedStatement2.setBigDecimal(3, credit.getRate());
            preparedStatement2.setDate(4, credit.getOpeningDate());
            preparedStatement2.setDate(5, credit.getClosingDate());
            preparedStatement2.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement2 != null) preparedStatement2.close();
            if (connection2 != null) connection2.close();
        }
    }

    @Override
    public int getLastCreditId() throws SQLException {
        Connection connection = getConnection();
        PreparedStatement preparedStatement = null;
        int quantity = 0;
        String sql = "SELECT * FROM credit ORDER BY id DESC limit 1;";
        try {
            preparedStatement = connection.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            quantity = resultSet.getInt("id");
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) preparedStatement.close();
            if (connection != null) connection.close();
        }
        return quantity;
    }

    @Override
    public void addCreditToUser(User user, Credit credit) throws SQLException {
        Connection connection = getConnection();
        PreparedStatement preparedStatementCheck = null;
        String sqlCheck = "SELECT * FROM user_has_credit where user_id=? and credit_id=?";
        try {
            preparedStatementCheck = connection.prepareStatement(sqlCheck);
            preparedStatementCheck.setInt(1, user.getId());
            preparedStatementCheck.setInt(2, credit.getId());
            ResultSet rs = preparedStatementCheck.executeQuery();
            if (!rs.next()) {
                PreparedStatement preparedStatement = null;
                String sql = "INSERT INTO user_has_credit " +
                        "(user_id,credit_id) " +
                        "VALUES (?,?);";
                try {
                    preparedStatement = connection.prepareStatement(sql);
                    preparedStatement.setInt(1, user.getId());
                    preparedStatement.setInt(2, credit.getId());
                    preparedStatement.executeUpdate();
                } catch (SQLException e) {
                    e.printStackTrace();
                } finally {
                    if (preparedStatement != null) preparedStatement.close();
                }
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatementCheck != null) preparedStatementCheck.close();
            if (connection != null) connection.close();
        }
    }

    @Override
    public void update(Credit credit) throws SQLException {
        Connection connection = getConnection();
        PreparedStatement preparedStatement = null;
        String sql = "UPDATE credit SET " +
                "sum=?,currency_id=?,rate=?,opening_date=?,closing_date=? WHERE id=?;";
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(6, credit.getId());
            preparedStatement.setBigDecimal(1, credit.getSum());
            preparedStatement.setInt(2, credit.getCurrency().getId());
            preparedStatement.setBigDecimal(3, credit.getRate());
            preparedStatement.setDate(4, credit.getOpeningDate());
            preparedStatement.setDate(5, credit.getClosingDate());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) preparedStatement.close();
            if (connection != null) connection.close();
        }
    }
}
