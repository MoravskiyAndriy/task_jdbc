package com.moravskiyandriy.logo.pixels.implementation;

import com.moravskiyandriy.logo.pixels.Pixel;

public class BlackPixel implements Pixel {

    @Override
    public String getColorPixel() {
        return "\033[0;30m" + " █";
    }
}
