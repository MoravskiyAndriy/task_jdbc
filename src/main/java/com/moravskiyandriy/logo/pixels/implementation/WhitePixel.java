package com.moravskiyandriy.logo.pixels.implementation;

import com.moravskiyandriy.logo.pixels.Pixel;

public class WhitePixel implements Pixel {

    @Override
    public String getColorPixel() {
        return "\033[1;90m" + " █";
    }
}
