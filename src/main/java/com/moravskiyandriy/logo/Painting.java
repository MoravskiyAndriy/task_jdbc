package com.moravskiyandriy.logo;

import com.moravskiyandriy.logo.pixels.Pixel;

class Painting {
    private Pixel pixel;

    Painting(Pixel pixel) {
        this.pixel = pixel;
    }

    String getPixel() {
        return pixel.getColorPixel();
    }
}
