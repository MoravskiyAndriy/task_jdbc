package com.moravskiyandriy.extraservice;

import com.moravskiyandriy.businessLogic.AccountManager;
import com.moravskiyandriy.entities.Currency;

import java.math.BigDecimal;

public enum Ticket {
    TO_EUROPE(new BigDecimal(10), new AccountManager().getCurrencyByAbbreviation("EUR"), "To Europe"),
    TO_AMERICA(new BigDecimal(20), new AccountManager().getCurrencyByAbbreviation("USD"), "To America"),
    TO_UKRAINE(new BigDecimal(50), new AccountManager().getCurrencyByAbbreviation("UAH"), "To Ukraine");
    private BigDecimal price;
    private Currency currency;
    private String comment;

    Ticket(BigDecimal price, Currency currency, String comment) {
        this.price = price;
        this.currency = currency;
        this.comment = comment;
    }

    public BigDecimal getPrice() {
        return this.price;
    }

    public Currency getCurrency() {
        return this.currency;
    }

    @Override
    public String toString() {
        return "Ticket{" + comment +
                ", price=" + price +
                " " + currency.getCurrencyVal() +
                '}';
    }
}
