package com.moravskiyandriy.controller;

import com.moravskiyandriy.bankdeposit.deposit.DepositType;
import com.moravskiyandriy.bankdeposit.depositfabric.DepositSupplier;
import com.moravskiyandriy.entities.*;
import com.moravskiyandriy.extraservice.Ticket;

import java.math.BigDecimal;
import java.util.List;

public interface BankController {
    List<Account> getAccounts();

    List<Transaction> getUserTransactions();

    void replenishAccountBalance(BigDecimal sum, Currency currency, String comment);

    boolean replenishPhoneBalance(BigDecimal sum, String number);

    boolean transferToThisBankAccount(Long accountTo, BigDecimal sum);

    boolean transferToAnotherBankAccount(Long accountTo, BigDecimal sum);

    void changeAccountCurrency(Currency currency);

    void setUser(User user);

    void setCurrentAccount(Account account);

    void setCurrentDeposit(Deposit deposit);

    void setCurrentCredit(Credit credit);

    boolean checkNoUser();

    Currency getCurrencyByAbbreviation(String abbreviation);

    Long getCurrentAccountNumber();

    boolean addAccount(Currency currency);

    boolean deleteAccount();

    boolean accountIsEmpty();

    List<Deposit> getDeposits();

    List<Credit> getCredits();

    boolean addDeposit(DepositSupplier supplier, DepositType term, BigDecimal sum, Currency currency);

    boolean addCredit(BigDecimal sum);

    boolean terminateDeposit();

    void buyTicket(Ticket ticket);

    boolean payOffCredit(BigDecimal sum);

    void addCreditToUser(String phoneNumber);

    BigDecimal getCommissionByOperationId(int id);

    List<Currency> getCurrenciesList();
}
