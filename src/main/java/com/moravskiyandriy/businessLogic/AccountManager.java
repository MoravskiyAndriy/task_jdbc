package com.moravskiyandriy.businessLogic;

import com.moravskiyandriy.entities.*;
import com.moravskiyandriy.entities.Currency;
import com.moravskiyandriy.services.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

public class AccountManager {
    private static final BigDecimal MIN_SUM = new BigDecimal(0.01);
    private static final int MAX_ACCOUNT_NUMBER = Optional.
            ofNullable(getProperties().getProperty("MAX_ACCOUNT_NUMBER")).
            map(Integer::valueOf).orElse(Constants.MAX_ACCOUNT_NUMBER);
    private static final Logger logger = LogManager.getLogger(AccountManager.class);

    public List<Account> getUserAccounts(User user) {
        try {
            return new AccountService().getUserAccountsById(user.getId());
        } catch (SQLException e) {
            return new ArrayList<>();
        }
    }

    public boolean addAccount(User user, Currency currency) {
        int accountNumber = MAX_ACCOUNT_NUMBER;
        try {
            accountNumber = new AccountService().getUserAccountsQuantityById(user.getId());
        } catch (SQLException e) {
            logger.info("Some issues with database occurred. We are trying to solve the problem...");
            e.printStackTrace();
        }
        if (accountNumber < MAX_ACCOUNT_NUMBER) {
            addNewAccount(user, currency);
            return true;
        } else {
            return false;
        }
    }

    private void addNewAccount(User user, Currency currency) {
        Long lastNumber = null;
        try {
            lastNumber = new AccountService().getLastAccountNumber() + 1;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Account nAccount = new Account();
        nAccount.setUserId(user.getId());
        nAccount.setBalance(new BigDecimal(0));
        nAccount.setNumber(lastNumber);
        nAccount.setCurrency(currency);
        try {
            new AccountService().add(nAccount);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean deleteAccount(User user, Account account) {
        int accountNumber = Constants.MAX_ACCOUNT_NUMBER;
        try {
            accountNumber = new AccountService().getUserAccountsQuantityById(user.getId());
        } catch (SQLException e) {
            logger.info("Some issues with database occurred. We are trying to solve the problem...");
            e.printStackTrace();
        }
        if (accountNumber != 1) {
            try {
                new AccountService().delete(account);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return true;
        } else {
            return false;
        }
    }

    public void changeAccountCurrency(Account account, Currency currency) {
        account.setBalance(account.getBalance().divide(account.getCurrency().
                getToUAHModifier(), Constants.FORMAT_CONSTANT, RoundingMode.HALF_UP));
        account.setBalance(account.getBalance().multiply(currency.getToUAHModifier()));
        account.setBalance(account.getBalance().setScale(Constants.FORMAT_CONSTANT, RoundingMode.HALF_UP));
        account.setCurrency(currency);
        try {
            new AccountService().update(account);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean accountIsEmpty(Account account) {
        return account.getBalance().compareTo(MIN_SUM) < 0;
    }

    public List<Transaction> getUserTransactions(User user) {
        return user.getAccounts().stream().flatMap(s -> {
            List<Transaction> t = new LinkedList<>();
            try {
                t = new TransactionService().getAccountTransactions(s);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return t.stream();
        }).collect(Collectors.toCollection(LinkedList::new));
    }

    public void replenishBankAccountBalance(Account account, BigDecimal sum, Currency currency, String comment) {
        Currency currentCurrency = account.getCurrency();
        account.setBalance(account.getBalance().divide(currentCurrency.getToUAHModifier(),
                Constants.FORMAT_CONSTANT, RoundingMode.HALF_UP));
        account.setBalance(account.getBalance().add(sum.divide(currency.getToUAHModifier(),
                Constants.FORMAT_CONSTANT, RoundingMode.HALF_UP)));
        account.setBalance(account.getBalance().multiply(currentCurrency.getToUAHModifier()));
        account.setBalance(account.getBalance().setScale(Constants.FORMAT_CONSTANT, RoundingMode.HALF_UP));
        Operation operation = new Operation();
        try {
            operation = new OperationService().getOperationById(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Transaction transaction = formTransaction(account, sum, currency, comment);
        transaction.setOperation(operation);
        try {
            new AccountService().update(account);
            new TransactionService().addAccountTransaction(transaction);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private Transaction formTransaction(Account account, BigDecimal sum, Currency currency, String comment) {
        Transaction transaction = new Transaction();
        transaction.setAccountNumberThis(account.getNumber());
        transaction.setOperationCommentary(comment);
        transaction.setSum(sum);
        transaction.setCurrency(currency);
        transaction.setDate(new java.sql.Timestamp(new Date().getTime()));
        return transaction;
    }

    public boolean exhaustBankAccountBalance(Account account, BigDecimal sum, int operationId, String comment) {
        BigDecimal fee = new BigDecimal(0);
        try {
            if (account.getBalance().compareTo(sum
                    .add(new OperationService().getOperationById(operationId).getCommission().
                            multiply(account.getCurrency().getToUAHModifier()))) < 0) {
                logger.info("You don't have enough money.");
                return false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Currency currentCurrency = account.getCurrency();
        try {
            account.setBalance(account.getBalance().subtract(sum));
            account.setBalance(account.getBalance().divide(currentCurrency.getToUAHModifier(),
                    Constants.FORMAT_CONSTANT, RoundingMode.HALF_UP));
            account.setBalance(account.getBalance().subtract(new OperationService().
                    getOperationById(operationId).getCommission()));
            account.setBalance(account.getBalance().multiply(currentCurrency.getToUAHModifier()));
            account.setBalance(account.getBalance().setScale(2, RoundingMode.HALF_UP));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Operation operation = new Operation();
        try {
            operation = new OperationService().getOperationById(operationId);
            fee = operation.getCommission();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (operationId == Constants.TRANSFER_TO_THIS_BANK_ACCOUNT) {
            try {
                Account anotherAccount = new AccountService().getAccountByNumber(Long.parseLong(comment));
                if (anotherAccount.getUserId() == 0) {
                    logger.info("No such account exists.");
                    return false;
                }
                replenishBankAccountBalance(anotherAccount, sum,
                        account.getCurrency(), account.getNumber().toString());
            } catch (SQLException e) {
                logger.info("No such account exists.");
                return false;
            }
        }
        Transaction transaction = formTransaction(account, sum.add(fee.multiply(currentCurrency.
                getToUAHModifier())).negate(), currentCurrency, comment);
        transaction.setOperation(operation);
        try {
            new AccountService().update(account);
            new TransactionService().addAccountTransaction(transaction);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    public Currency getCurrencyByAbbreviation(String abbreviation) {
        Currency currency = Constants.UAH_Currency;
        try {
            currency = new CurrencyService().getCurrencyByAbbreviation(abbreviation);
        } catch (SQLException e) {
            logger.info("No such currency found.");
        }
        return currency;
    }

    public List<Currency> getCurrenciesList() {
        List<Currency> currencies = new ArrayList<>();
        try {
            return new CurrencyService().getAllCurrency();
        } catch (SQLException e) {
            logger.info("No currency found.");
        }
        return currencies;
    }

    public BigDecimal getCommissionByOperationId(int id){
        BigDecimal commission=new BigDecimal(0);
        try {
            commission = new OperationService().getOperationById(id).
                    getCommission().setScale(2,RoundingMode.HALF_UP);
        } catch (SQLException e) {
            logger.info("No such operation found.");
        }
        return commission;
    }

    private static Properties getProperties() {
        Properties prop = new Properties();
        try (InputStream input = AccountManager.class
                .getClassLoader().getResourceAsStream("config")) {
            if (input != null) {
                prop.load(input);
            }
        } catch (NumberFormatException ex) {
            if (Objects.nonNull(logger)) {
                logger.warn("NumberFormatException found.");
            }
        } catch (IOException ex) {
            if (Objects.nonNull(logger)) {
                logger.warn("IOException found.");
            }
        }
        return prop;
    }
}
