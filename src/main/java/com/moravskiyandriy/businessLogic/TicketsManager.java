package com.moravskiyandriy.businessLogic;

import com.moravskiyandriy.entities.Account;
import com.moravskiyandriy.extraservice.Ticket;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.math.RoundingMode;
import java.util.Optional;
import java.util.Properties;

public class TicketsManager {
    private static final Logger logger = LogManager.getLogger(TicketsManager.class);

    public boolean buyTicket(Ticket ticket, Account account) {
        return new AccountManager().exhaustBankAccountBalance(account, ticket.getPrice().
                        divide(ticket.getCurrency().getToUAHModifier(), Constants.FORMAT_CONSTANT,
                                RoundingMode.HALF_UP).multiply(account.getCurrency().
                        getToUAHModifier().setScale(Constants.FORMAT_CONSTANT, RoundingMode.HALF_UP)),
                Constants.TRANSFER_TO_ANOTHER_BANK_ACCOUNT, Optional.ofNullable(getProperties().
                        getProperty("UKR_GAS_BANK_ACCOUNT_NUMBER")).
                        orElse(Constants.UKR_GAS_BANK_ACCOUNT_NUMBER.toString()));
    }

    private static Properties getProperties() {
        Properties prop = new Properties();
        try (InputStream input = AccountManager.class
                .getClassLoader().getResourceAsStream("config")) {
            if (input != null) {
                prop.load(input);
            }
        } catch (NumberFormatException ex) {
            logger.warn("NumberFormatException found.");
        } catch (IOException ex) {
            logger.warn("IOException found.");
        }
        return prop;
    }
}
